#!/usr/bin/python3

from gpiozero import LED
from time import sleep

led = LED(25)


def blink(num_times, sleep_time):
    for i in range(0, num_times):
        print("Iteration " + str(i + 1))
        led.on()
        sleep(sleep_time)
        led.off()
        sleep(sleep_time)
    print("Done")


iterations = input("Enter total number of time to blink: ")
speed = input("Enter length of each blink(seconds): ")
blink(int(iterations), float(speed))
